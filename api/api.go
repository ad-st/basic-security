package api

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net/http"
)

//Credentials provides key to the system. Use Key publicly as your identifier. Keep SharedSecret safe for signing only.
type Credentials struct {
	Key          [16]byte
	SharedSecret [64]byte
}

//Generate will create new set of credentials
func Generate() Credentials {
	credentials := Credentials{}
	rand.Read(credentials.Key[:])
	rand.Read(credentials.SharedSecret[:])
	return credentials
}

//SignData will create signature for specific data using credentials
func SignData(credentials Credentials, data string) []byte {
	hash := sha256.New()
	hash.Write(credentials.Key[:])
	hash.Write(credentials.SharedSecret[:])
	hash.Write([]byte(data))
	return hash.Sum(nil)
}

//SignHeaders will create appropriate headers for communication
func SignHeaders(req *http.Request, credentials *Credentials) {
	if credentials != nil {
		signature := SignData(*credentials, req.URL.Path)
		req.Header.Add("X-API-Key", hex.EncodeToString(credentials.Key[:]))
		req.Header.Add("X-Signature", hex.EncodeToString(signature))

		fmt.Println(hex.EncodeToString(credentials.Key[:]))
		fmt.Println(hex.EncodeToString(signature))
		fmt.Println(req.URL.Path)
	}
}

//ExtractSignatureFromHeaders returns api key and siganture from http request
func ExtractSignatureFromHeaders(req *http.Request) (key [16]byte, signature []byte) {
	receivedAPIKey, _ := hex.DecodeString(req.Header.Get("X-API-Key"))
	receivedSignature, _ := hex.DecodeString(req.Header.Get("X-Signature"))

	var receivedAPIKeyArr [16]byte
	copy(receivedAPIKeyArr[:], receivedAPIKey)

	return receivedAPIKeyArr, receivedSignature
}
