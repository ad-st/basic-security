package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/ad-st/basic-security/api"
)

func main() {
	connection := createSecureConnectionWithServer("../server/localhost.crt")
	credentials := requestNewAPIKey(connection)
	createStaticPage("localhost", "./secrets.html", credentials)
}

func createStaticPage(host string, fileName string, credentials api.Credentials) {
	data := struct {
		ApiKey    string
		Signature string
		Txt       string
		Url       string
	}{
		ApiKey:    hex.EncodeToString(credentials.Key[:]),
		Signature: hex.EncodeToString(api.SignData(credentials, "/secrets/7")),
		Txt:       "Get secret 7",
		Url:       "https://" + host + "/secrets/7",
	}
	t, _ := template.ParseFiles("index.tpl")

	w, _ := os.Create(fileName)
	t.Execute(w, &data)
	w.Close()

	fmt.Printf("Generated controlling page for host: %s in %s\n", host, fileName)
}

func createSecureConnectionWithServer(serverCertPath string) *http.Client {
	var trustedRootCertificates = getTrustedRootCertificates(serverCertPath)
	return createClientConnection(trustedRootCertificates)
}

func getTrustedRootCertificates(certFile string) *x509.CertPool {
	pemCert, err := ioutil.ReadFile(certFile)
	if err != nil {
		panic(err)
	}

	trustedRootCertificates := x509.NewCertPool()
	trustedRootCertificates.AppendCertsFromPEM(pemCert)

	return trustedRootCertificates
}

func createClientConnection(trustedRootCertificates *x509.CertPool) *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs: trustedRootCertificates,
		},
	}
	client := &http.Client{Transport: tr}
	return client
}

func requestNewAPIKey(client *http.Client) api.Credentials {
	req, _ := http.NewRequest("POST", "https://localhost/keys", nil)
	response, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	content, _ := ioutil.ReadAll(response.Body)

	credentials := api.Credentials{}
	json.Unmarshal(content, &credentials)

	fmt.Printf("Accquired new credentials, key: %v\n", credentials.Key)
	return credentials
}
