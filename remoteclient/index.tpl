<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script>
        const makeRequest = (method, url, apiKey, signature) => {
            var xhr = new XMLHttpRequest();
            xhr.open(method, url);
            xhr.setRequestHeader("X-API-Key", apiKey);
            xhr.setRequestHeader("X-Signature", signature);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    var data = document.getElementById("data")
                    data.innerHTML += xhr.responseText + "<br>"
                } else {
                    alert("error");
                }
            };
            xhr.send();
        }
    </script>

</head>

<body>
<button onclick="makeRequest('GET','{{.Url}}', '{{.ApiKey}}', '{{.Signature}}')">{{.Txt}}</button>
<div id="data">
</div>
</body>