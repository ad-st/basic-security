package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/ad-st/basic-security/api"
)

func main() {
	connection := createSecureConnectionWithServer("../server/localhost.crt")
	credentials := requestNewAPIKey(connection)
	secret := getProtectedData(connection, credentials)
	fmt.Println(secret)
}

func createSecureConnectionWithServer(serverCertPath string) *http.Client {
	var trustedRootCertificates = getTrustedRootCertificates(serverCertPath)
	return createClientConnection(trustedRootCertificates)
}

func getTrustedRootCertificates(certFile string) *x509.CertPool {
	pemCert, err := ioutil.ReadFile(certFile)
	if err != nil {
		panic(err)
	}

	trustedRootCertificates := x509.NewCertPool()
	trustedRootCertificates.AppendCertsFromPEM(pemCert)

	return trustedRootCertificates
}

func createClientConnection(trustedRootCertificates *x509.CertPool) *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs: trustedRootCertificates,
		},
	}
	client := &http.Client{Transport: tr}
	return client
}

func requestNewAPIKey(client *http.Client) api.Credentials {
	req, _ := http.NewRequest("POST", "https://localhost/keys", nil)
	response, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	content, _ := ioutil.ReadAll(response.Body)

	credentials := api.Credentials{}
	json.Unmarshal(content, &credentials)
	return credentials
}

func getProtectedData(client *http.Client, credentials api.Credentials) string {
	req, _ := http.NewRequest("GET", "https://localhost/secrets/7", nil)
	api.SignHeaders(req, &credentials)
	response, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	if response.StatusCode != 200 {
		return "No secrets"
	}
	defer response.Body.Close()
	content, _ := ioutil.ReadAll(response.Body)

	return string(content)
}
