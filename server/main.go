package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"

	"github.com/gorilla/mux"
	"gitlab.com/ad-st/basic-security/api"
)

var registeredClientsMtx = sync.Mutex{}
var registeredClients = make(map[[16]byte][64]byte)

func getSharedSecret(key [16]byte) ([64]byte, bool) {
	registeredClientsMtx.Lock()
	elem, ok := registeredClients[key]
	registeredClientsMtx.Unlock()
	return elem, ok
}

func createNewCredentials() api.Credentials {
	credentials := api.Generate()
	registeredClientsMtx.Lock()
	registeredClients[credentials.Key] = credentials.SharedSecret
	registeredClientsMtx.Unlock()
	return credentials
}

func getValidCredentials(key [16]byte) (api.Credentials, bool) {
	foundSecret, ok := getSharedSecret(key)
	credentials := api.Credentials{
		Key:          key,
		SharedSecret: foundSecret,
	}
	return credentials, ok
}

func validationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		receivedAPIKey, receivedSignature := api.ExtractSignatureFromHeaders(r)
		validCredentials, credentialsFound := getValidCredentials(receivedAPIKey)
		validSignature := api.SignData(validCredentials, r.URL.Path)

		if !credentialsFound || !bytes.Equal(validSignature, receivedSignature) {
			http.Error(w, "Unauthorized", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
			w.Header().Add("Access-Control-Allow-Headers", "Content-Type, X-Signature, X-API-Key")
			w.Header().Set("Access-Control-Allow-Methods", "GET, PUT")
		} else {
			w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
			next.ServeHTTP(w, r)
		}
	})
}

func requestNewAPIKey(w http.ResponseWriter, r *http.Request) {
	credentials := createNewCredentials()
	result, _ := json.Marshal(credentials)
	w.Write(result)
}

func getSecret(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fmt.Fprintf(w, "Hello Secret %s World", vars["dataId"])
}

func main() {
	router := mux.NewRouter()
	router.Use(corsMiddleware)
	router.HandleFunc("/keys", requestNewAPIKey).Methods("POST")

	secureRouter := mux.NewRouter()
	secureRouter.Use(validationMiddleware)
	secureRouter.HandleFunc("/secrets/{dataId}", getSecret).Methods("GET")

	router.Handle("/secrets/{dataId}", secureRouter)

	http.ListenAndServeTLS(":443", "localhost.crt", "localhost.key", router)
}
